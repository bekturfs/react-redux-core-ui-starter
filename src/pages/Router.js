import React, {Suspense, lazy} from "react"
import {Route, Switch} from "react-router-dom"
import PrivateRoute from "../containers/PrivateRoute"
import FullSpinner from "../components/spinners/FullSpinner"

const LoginPage = lazy(() => import('./LoginPage'))
const RegisterPage = lazy(() => import('./RegisterPage'))
const MainPage = lazy(() => import('./MainPage'))
const Page404 = lazy(() => import('./Page404'))

export default function Router(){
  return (
    <Suspense fallback={<FullSpinner/>}>
      <Switch>
        <Route exact path="/login">
          <LoginPage/>
        </Route>
        <Route exact path="/register">
          <RegisterPage/>
        </Route>
        <PrivateRoute exact path="/main">
          <MainPage/>
        </PrivateRoute>
        <Route exact path="/">
          <MainPage/>
        </Route>
        <Route>
          <Page404/>
        </Route>
      </Switch>
    </Suspense>
  )
}
